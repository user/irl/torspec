#!/usr/bin/env bash

set -euo pipefail

set -x

# usage:
#   bin/via-yarn-install-in-ci COMMAND GIT-URL-FOR-PACKAGE COMMITID
#
# We demand a commitid to mitigate the security risks of npm

command="$1"
git_url="$2"
commitid="$3"

# Bump this to "clear" the cache.
# Actually, it just causes us to ignore previous cached results.
cache_clear_token=2023-11-09c

cache_dir=cache/"$CI_JOB_IMAGE","$cache_clear_token","$command","$commitid"
local_dir="yarn-build,$command"

mkdir -p "$local_dir"

if test -d "$cache_dir"/; then
    cp -al "$cache_dir"/. "$local_dir"/.

    type -p node || apt-get install -y nodejs
else

    if [ -z "${YARN-}" ]; then
	type -p yarnpkg || apt-get install -y yarnpkg
	YARN=yarnpkg
    fi

    cd "$local_dir"
    git clone "$git_url" "$command".git
    cd "$command".git
    git config advice.detachedHead false
    git checkout "$commitid"

    : ----- invoke yarn to build "$command": running -----
    $YARN install --frozen-lockfile --non-interactive
    : ----- invoke yarn to build "$command": complete -----

    cd ../..
    ls -al "$PWD"/"$local_dir"/"$command".git/src/cli.js

    mkdir -p "$cache_dir"
    cp -al "$local_dir"/. "$cache_dir"/.
fi

# We abuse $CARGO_HOME/bin.
# The rust: CI images we're using have that on PATH.
# If one were to run this outside CI, putting a symlink
# to some NPM thing in ~/.cargo/bin doesn't seem terrible.

ln -s "$PWD"/"$local_dir"/"$command".git/src/cli.js \
   "$CARGO_HOME"/bin/"$command"
